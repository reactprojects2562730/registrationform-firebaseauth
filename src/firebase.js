import { initializeApp } from "firebase/app";
import {getAuth} from 'firebase/auth'

const firebaseConfig = {
  apiKey: "AIzaSyAWY1aM6V_xNJaKh4C3r71Y1iTjNkjsYPI",
  authDomain: "fir-auth-77.firebaseapp.com",
  projectId: "fir-auth-77",
  storageBucket: "fir-auth-77.appspot.com",
  messagingSenderId: "689410625173",
  appId: "1:689410625173:web:084416bbcbb1c0c07bd59c",
  measurementId: "G-74QEGZFBF6"
};


const app = initializeApp(firebaseConfig);
const auth = getAuth();


export {app, auth};