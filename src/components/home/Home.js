import React from 'react'
import { Link } from 'react-router';

function Home(props) {
  return (
    <div>
      <div>
        <h1>
            <a href="/login">Login</a>
        </h1>
        <br />
        <h1>
            <a href="/signup">SignUp</a>
        </h1>
      </div>

      <br />
      <br />
      <br />
      
      <h2>{props.name ? `Welcome - ${props.name}` : "Login Please"}</h2>
    </div>
  )
}

export default Home
