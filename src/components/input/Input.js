import React from 'react'
import css from './input.module.css'

function Input(props) {
  return (
    <div className={css.container}>
        {props.label && <label>{props.label}</label>}
        <input type="mail" {...props}/>
    </div>
  )
}

export default Input
