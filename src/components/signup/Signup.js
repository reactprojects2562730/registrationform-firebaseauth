import React, { useState } from "react";
import css from "./Signup.module.css";
import Input from "../input/Input";
import { createUserWithEmailAndPassword, updateProfile } from "firebase/auth";
import { auth } from "../../firebase";
import { useNavigate } from "react-router-dom";

function Signup() {
  const [values, setValues] = useState({
    name: "",
    email: "",
    pass: "",
  });

  const [error, setError] = useState("");
  const [disableSubmit, setDisableSubmit] = useState(false);

  const handleSubmit = () => {
    if (!values.name) {
      setError("Name is required");
      return;
    } else if (!values.email) {
      setError("Email is required");
      return;
    } else if (!values.pass) {
      setError("Password is required");
      return;
    }
    setError("");

    setDisableSubmit(true);
    createUserWithEmailAndPassword(auth, values.email, values.pass)
      .then(async(res) => {
        setDisableSubmit(false);
        const user = res.user;
        await updateProfile(user, {
          displayName: values.name
        });
        console.log(user)
      })
      .catch((err) => {
        setDisableSubmit(false);
        setError(err.message);
        console.log("Error", err.message);
      });
      navigate("/")
  };

  const navigate = useNavigate();

  return (
    <div className={css.container}>
      <div className={css.innerBox}>
        <h1 className={css.heading}>SignUp</h1>

        <Input
          label="Name"
          placeholder="Enter Your Name"
          onChange={(event) =>
            setValues((prev) => ({ ...prev, name: event.target.value }))
          }
        />
        <Input
          label="Email"
          placeholder="Enter Email Address"
          onChange={(event) =>
            setValues((prev) => ({ ...prev, email: event.target.value }))
          }
        />
        <Input
          label="Password"
          placeholder="Enter Password"
          onChange={(event) =>
            setValues((prev) => ({ ...prev, pass: event.target.value }))
          }
        />

        <div className={css.footer}>
          <b className={css.error}>{error}</b>
          <button onClick={handleSubmit} disabled={disableSubmit}>
            SignUp
          </button>
          <p>
            Already have an Account?
            <span>
              <a href="/login">Login</a>
            </span>
          </p>
        </div>
      </div>
    </div>
  );
}

export default Signup;
