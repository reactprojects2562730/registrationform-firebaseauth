import React from "react";
import css from "./Login.module.css";
import Input from "../input/Input";
import { signInWithEmailAndPassword} from "firebase/auth";
import { auth } from "../../firebase";
import { useNavigate } from "react-router-dom";
import { useState } from "react";

function Login() {
  const [values, setValues] = useState({
    email: "",
    pass: "",
  });

  const [error, setError] = useState("");
  const [disableSubmit, setDisableSubmit] = useState(false);

  const handleSubmit = () => {
    if (!values.email) {
      setError("Email is required");
      return;
    } else if (!values.pass) {
      setError("Password is required");
      return;
    }
    setError("");

    setDisableSubmit(true);
    signInWithEmailAndPassword(auth, values.email, values.pass)
      .then(async (res) => {
        setDisableSubmit(false);
        const user = res.user;
        // await updateProfile(user, {
        //   displayName: values.name,
        // });
        console.log(user);
      })
      .catch((err) => {
        setDisableSubmit(false);
        setError(err.message);
        console.log("Error", err.message);
      });
    navigate("/");
  };

  const navigate = useNavigate();

  return (
    <div className={css.container}>
      <div className={css.innerBox}>
        <h1 className={css.heading}>Login</h1>

        <Input
          label="Email"
          placeholder="Enter Email Address"
          onChange={(event) =>
            setValues((prev) => ({ ...prev, email: event.target.value }))
          }
        />
        <Input
          label="Password"
          placeholder="Enter Password"
          onChange={(event) =>
            setValues((prev) => ({ ...prev, pass: event.target.value }))
          }
        />

        <div className={css.footer}>
          <b className={css.erroe}>{error}</b>
          <button onClick={handleSubmit} disabled={disableSubmit}>
            Login
          </button>
          <p>
            Don't have an Account?
            <span>
              <a href="/signup">Sign Up</a>
            </span>
          </p>
        </div>
      </div>
    </div>
  );
}

export default Login;
